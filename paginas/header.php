<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/style.css" type="text/css">
    <link rel="stylesheet" href="../css/dadosform.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link   href="https://fonts.googleapis.com/css?family=Asap" rel="stylesheet">
    <link   href="https://fonts.googleapis.com/css?family=Bitter" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script type="text/javascript" src="../js/main.js"></script>
    <script type="text/javascript" src="../js/plugins/jquery.mask.js"></script>
    <title>Sistema TXCorrida</title>
</head>
<body>
        <header>
            <div class="fixed">
                <div class="menu">
                    <nav>
                        <ul>
                            <li><a class="navbar-brand" href="../index.html">Inicio</a></li>
                            <li><a class="navbar-brand" href="../motorista/index.html">Cadastro Motorista</a></li>
                            <li><a class="navbar-brand" href="../passageiro/index.html">Cadastro Passageiro</a></li>
                            <li><a class="navbar-brand" href="../corridas/index.php">Corridas</a></li>
                            <li><a class="navbar-brand" href="../dados/index.php">Dados</a></li>
                        </ul>
                    </nav>
                </div>
        </div>
        </header>